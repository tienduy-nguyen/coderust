class ListNode {
  constructor(val, next = null, prev = null) {
    this.val = val;
    this.next = next;
    this.prev = prev;
  }
}
class DoubleLinkedList {
  constructor(head = null) {
    this.head = head;
  }
}
